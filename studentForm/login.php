<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Intro PHP - Login and Control Page</title>

<link rel="stylesheet" type="text/css" href="studentForm.css">

</head>

<body>

<div>

<h1>WDV341 Intro PHP</h1>

			<h2>Please login to the Administrator System</h2>
                <form id="studentForm" name="studentForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                <label for="userName">User Name</label>
    			<input type="text" id="userName" name="userName" required="required">

    			<label for="password">Password</label>
    			<input type="password" id="password" name="password" required="required">
                <input type="submit" name="submit" id="submit" value="Submit Info">
    			<input type="reset" id="reset" value="Clear Form" >
                </form>

</div>
</body>
</html>